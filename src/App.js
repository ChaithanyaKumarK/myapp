import './App.css';
import React from 'react';
import { Route, Routes } from "react-router-dom";
import MainPage from './pages/MainPage';
import ProductDetails from './pages/ProductDetails';

function App() {
  return (
    <>
      <Routes>
        <Route
          path="productDetails/:id"
          key="ProductDetails"
          element={<ProductDetails />}
        ></Route>
        <Route
          path="/"
          key="mainPage"
          element={<MainPage />}
        ></Route>

      </Routes>


    </>
  );
}

export default App;
