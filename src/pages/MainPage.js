import React, { useEffect, useState } from "react";
import ProductTab from "../components/ProductTab"
import axios from "axios";

const MainPage = () => {
    let [products, setProducts] = useState({});
    let [loading, setLoading] = useState(true);
    const [isFormOpen, setIsFormOpen] = useState(false);
    useEffect(() => {
        fetchProducts();
    }, [])


    const fetchProducts = () => {
        setLoading(true)
        axios.get("https://fakestoreapi.com/products")
            .then((response) => {
                setProducts(response.data)
                setLoading(false)
            })
    }

    const displayEachproduct = (products) => {

        return (products && products.length > 0) ?
            products.map(element => {
                return <ProductTab data={element} key={element.id} />
            }) : <></>
    }

    const openForm = () => {
        setIsFormOpen(true);
    }
    const closeForm = () => {
        setIsFormOpen(false);
    }

    const createProduct = (body) => {
        setLoading(true)
        axios({
            method: 'post',
            url: `https://fakestoreapi.com/products`,
            data: JSON.stringify(body)
        })
            .then((response) => {
                let newArr = [];
                let id = response.data.id
                body["id"] = id
                newArr.push(body)
                setProducts(products.concat(newArr));
                setIsFormOpen(false);
                setLoading(false)
            })
            .catch((err) => {
                console.error(err);
            })
    }
    const submitFn = (e) => {
        let submitBody = {};
        submitBody["title"] = e.target.title.value;
        submitBody["price"] = Number(e.target.price.value);
        submitBody["description"] = e.target.description.value;
        submitBody["image"] = e.target.image.value;
        submitBody["category"] = e.target.category.value;

        createProduct(submitBody);
    }

    return (
        <div className="d-flex flex-row flex-wrap justify-content-around ">
            {loading ?<div className=" d-flex justify-content-center align-items-center " style={{ width: "100vw", height: "100vh" }}><h1>loading...</h1></div> : isFormOpen ?
                <>
                    <div className=" d-flex justify-content-center align-items-center " style={{ width: "100vw", height: "100vh" }}>
                        <form onSubmit={(e) => {
                            e.preventDefault();
                            submitFn(e);
                        }}>
                            <div className="form-group">
                                <label for="title">Title</label>
                                <input type="text" required className="form-control" id="title" />
                            </div>
                            <div className="form-group">
                                <label for="price">Price</label>
                                <input type="number" required className="form-control" id="price" />
                            </div>
                            <div className="form-group">
                                <label for="description">Description</label>
                                <input type="textbox" required className="form-control" id="description" />
                            </div>
                            <div className="form-group">
                                <label for="image">Image</label>
                                <input type="text" className="form-control" id="image" />
                            </div>
                            <div className="form-group">
                                <label for="category">category</label>
                                <input type="text" className="form-control" id="category" />
                            </div>
                            <button type="button" className="btn btn-secondary mx-1" onClick={closeForm}>Cancel</button>
                            <button type="submit" className="btn btn-primary mx-1">Submit</button>
                        </form>
                    </div>

                </>

                :
                <>
                    <div className="d-flex justify-content-end px-2" style={{ width: "100vw"}}>
                        <button type="button" className="btn btn-primary m-1" onClick={openForm}>Create Product</button>
                    </div>
                    {displayEachproduct(products)}

                </>
            }
        </div>
    )
}

export default MainPage