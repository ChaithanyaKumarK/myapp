import { useLocation, useNavigate } from 'react-router-dom'
import { useState, useEffect } from "react"
import axios from "axios";

const ProductDetails = () => {
    const location = useLocation()
    const navigate = useNavigate();
    const id = Number(location.pathname.split("/")[2])
    const [product, setProduct] = useState({});
    const [loading, setLoading] = useState(true);
    const [isFormOpen, setIsFormOpen] = useState(false);

    useEffect(() => {
        getProduct();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const getProduct = () => {
        axios.get(`https://fakestoreapi.com/products/${id}`)
            .then((response) => {
                if (response && response.data) {
                    setProduct(response.data);
                    setLoading(false);
                }
                else {
                    console.error("no data found");
                    navigate('/')
                }
            }).catch((error) => {
                console.error(error);
            })
    }
    const openForm = () => {
        setIsFormOpen(true);
    }
    const closeForm = () => {
        setIsFormOpen(false);
    }
    const updateProduct = (body) => {
        setLoading(true)
        axios({
            method: 'put',
            url: `https://fakestoreapi.com/products/${id}`,
            data: JSON.stringify(body)
        })
            .then((response) => {
                setProduct(body);
                setIsFormOpen(false);
                setLoading(false)
            })
            .catch((err) => {
                console.error(err);
            })
    }

    const deleteProduct = () => {
        setLoading(true)
        axios({
            method: 'delete',
            url: `https://fakestoreapi.com/products/${id}`,
        })
            .then((response) => {
                setLoading(true)
                navigate('/')
            })
            .catch((err) => {
                console.error(err);
            })
    }
    const submitFn = (e) => {
        let submitBody = {};
        submitBody["title"] = e.target.title.value;
        submitBody["price"] = Number(e.target.price.value);
        submitBody["description"] = e.target.description.value;
        submitBody["image"] = e.target.image.value;
        submitBody["category"] = e.target.category.value;

        updateProduct(submitBody);
    }
    return (
        <div className='d-flex align-items-center justify-content-center' style={{ width: "100vw", height: "100vh" }}>
            {loading ? <h1>loading...</h1> : isFormOpen ?
                (
                    <div className='container p-6' style={{ width: "40vw", height: "auto" }}>
                    <form onSubmit={(e) => {
                        e.preventDefault();
                        submitFn(e);
                    }}>
                        <div className="form-group">
                            <label for="title">Title</label>
                            <input type="text" required className="form-control" id="title" defaultValue={product.title} />
                        </div>
                        <div className="form-group">
                            <label for="price">Price</label>
                            <input type="number" required className="form-control" id="price" defaultValue={product.price} />
                        </div>
                        <div className="form-group">
                            <label for="description">Description</label>
                            <input type="textbox" required className="form-control" id="description" defaultValue={product.description} />
                        </div>
                        <div className="form-group">
                            <label for="image">Image</label>
                            <input type="text" readonly disabled className="form-control" id="image" value={product.image} />
                        </div>
                        <div className="form-group">
                            <label for="category">category</label>
                            <input type="text" readonly disabled className="form-control" id="category" value={product.category} />
                        </div>
                        <button type="button" className="btn btn-secondary mx-1" onClick={closeForm}>Cancel</button>
                        <button type="submit" className="btn btn-primary mx-1">Submit</button>
                    </form>
                    </div>
                ) :
                (<div className='container'>
                    <div className='row'>
                        <div className='col-md-6'> <img src={product.image} className=' h-100 w-75' alt='product' /></div>
                        <div className='col-md-6'>
                            <h2>{product.title}</h2>
                            <p className="text-wrap">
                                {product.description}
                            </p>
                            <h2>Categories</h2>
                            <span className="badge rounded-pill bg-info text-dark mb-3 me-2">{product.category}</span>
                            <h2>Price</h2>
                            <h4>{product.price} $</h4>
                            <button type="button" className="btn btn-secondary mx-1" onClick={openForm}>Edit Item</button>
                            <button type="button" className="btn btn-danger mx-1" onClick={deleteProduct}>Delete Item</button>
                        </div>
                    </div>
                </div>)}
        </div>
    )

}

export default ProductDetails